package nguyen.khang.haegrouptest.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ServiceApi {
        @GET
        Call<List<CountryInfo>> loadCountryInfo(@Url String url);
}
