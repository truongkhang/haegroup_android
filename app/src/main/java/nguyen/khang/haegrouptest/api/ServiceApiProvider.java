package nguyen.khang.haegrouptest.api;

import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Khang on 10/26/2015.
 */
public class ServiceApiProvider {
    static Retrofit retrofit;
    static OkHttpClient httpClient;

    public static void createRetrofitClient(String serverUrl) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.interceptors().add(logging);

        GsonBuilder gsonBuilder = new GsonBuilder();

        httpClient = builder.build();
        retrofit = new Retrofit.Builder().baseUrl(serverUrl)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .build();
    }

    public static void cancelAll() {
        httpClient.dispatcher().cancelAll();
    }

    public static synchronized ServiceApi getApiService() {

        if (retrofit == null) {
            createRetrofitClient("https://restcountries.eu");
        }

        return retrofit.create(ServiceApi.class);
    }
}
