package nguyen.khang.haegrouptest.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class CountryInfo {
    @SerializedName("translations")
    Map<String, String> translations;

    @SerializedName("currencies")
    List<Currency> currencies;

    @SerializedName("capital")
    String capital;

    public String getCurrency() {
        String result = "";
        if (currencies != null && currencies.size() > 0) {
            for(int i = 0; i < currencies.size(); i++) {
                Currency currency = currencies.get(i);
                result += currency.code;
                if (i != currencies.size() -1 ) {
                    result += " - ";
                }
            }
        }

        return result;
    }

    public String getCapital() {
        return capital;
    }

    public String findTranslation(String countrycode) {
        if (translations != null && translations.containsKey(countrycode))
            return translations.get(countrycode);

        return null;
    }

    public static class Currency {
        @SerializedName("code")
        String code;
        @SerializedName("name")
        String name;
        @SerializedName("symbol")
        String symbol;
    }
}