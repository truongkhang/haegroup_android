package nguyen.khang.haegrouptest.api;

public interface ServiceCallback<TH> {
    void onSucceeded(TH data);
    void onFailed(String message);
}
