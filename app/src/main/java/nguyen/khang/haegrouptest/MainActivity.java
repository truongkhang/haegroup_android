package nguyen.khang.haegrouptest;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import nguyen.khang.haegrouptest.api.CountryInfo;
import nguyen.khang.haegrouptest.api.ServiceCallback;

public class MainActivity extends AppCompatActivity {
    int REQUEST_CODE_LOCATION_PERMISSION = 10;
    String TAG = MainActivity.class.getSimpleName();
    String EXTRA_COUNTRY = "country";
    String EXTRA_CAPITAL = "captital";
    String EXTRA_CURRENCY = "currency";

    TextView tvDate;
    TextView tvTime;
    TextView tvBattery;
    TextView tvStatus;
    ProgressBar progressBar;

    TextView tvCountry;
    TextView tvCapital;
    TextView tvCurrency;

    Timer timer;
    TimerTask timerTask;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
    SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
    Calendar calendar = Calendar.getInstance();

    BroadcastReceiver batteryReceiver;
    BroadcastReceiver locationReceiver;

    String loadedCountry;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(EXTRA_COUNTRY, tvCountry.getText().toString());
        outState.putString(EXTRA_CURRENCY, tvCurrency.getText().toString());
        outState.putString(EXTRA_CAPITAL, tvCapital.getText().toString());

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();

        stopTimer();
        if (batteryReceiver != null)
            unregisterReceiver(batteryReceiver);
        if (locationReceiver != null) {
            LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
            manager.unregisterReceiver(locationReceiver);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        startTimer();

        // battery
        batteryReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateBatteryInfo(intent);
            }
        };
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = registerReceiver(batteryReceiver, ifilter);
        updateBatteryInfo(batteryStatus);

        // location
        locationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateLocation(intent);
            }
        };
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.registerReceiver(locationReceiver, new IntentFilter(LocationService.INTENT_LOCATION));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupView();

        if (savedInstanceState != null) {
            tvCountry.setText(savedInstanceState.getString(EXTRA_COUNTRY));
            tvCapital.setText(savedInstanceState.getString(EXTRA_CAPITAL));
            tvCurrency.setText(savedInstanceState.getString(EXTRA_CURRENCY));
        }

        if (LocationService.hasLocationPermission(this)) {
            startService(new Intent(this, LocationService.class));
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_LOCATION_PERMISSION);
        }
    }

    void setupView() {

        tvDate = findViewById(R.id.tv_date);
        tvTime = findViewById(R.id.tv_time);
        tvBattery = findViewById(R.id.tv_battery);
        tvStatus = findViewById(R.id.tv_status);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.setMax(100);

        tvCountry = findViewById(R.id.tv_country);
        tvCapital = findViewById(R.id.tv_capital);
        tvCurrency = findViewById(R.id.tv_currency);

        findViewById(R.id.btn_launcher).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AppListActivity.class));
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           @NonNull final String permissions[], @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE_LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startService(new Intent(this, LocationService.class));
            } else {
            }
        }
    }

    void startTimer() {
        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                updateClock();
            }
        };
        timer.schedule(timerTask, 0, 500);
    }

    void stopTimer() {
        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    void updateClock() {
        tvDate.post(new Runnable() {
            @Override
            public void run() {
                calendar.setTimeInMillis(System.currentTimeMillis());
                Date curTime = calendar.getTime();
                tvDate.setText(simpleDateFormat.format(curTime));
                tvTime.setText(simpleTimeFormat.format(curTime));
            }
        });
    }

    void updateBatteryInfo(Intent batteryStatus) {
        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean isCharging = (chargePlug != 0);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        int batteryPercent = (int) (level * 100 / (float) scale);

        tvBattery.setText(String.format(Locale.ENGLISH, "%d%%", batteryPercent));
        tvStatus.setText(isCharging ? "Charging" : "Unplugged");
        progressBar.setProgress((int) batteryPercent);
    }

    int count = 0;

    void updateLocation(Intent intent) {
        Location location = intent.getExtras().getParcelable(LocationService.EXTRA_LOCATION);

        count++;
        Log.d(TAG, "Position updated count: " + count);

        loadCountry(location);
    }

    void loadCountry(Location location) {
        GeoAsyncTask asyncTask = new GeoAsyncTask(this, location);
        asyncTask.setListener(new GeoAsyncTask.Listener() {
            @Override
            public void onFinish(String country) {
                loadedCountry = (country);
                loadCountryDetail(country);
            }
        } );
        asyncTask.execute();
    }

    void loadCountryDetail(String country) {
        DataManager.getInstance().loadCountryDetail(country, new ServiceCallback<CountryInfo>() {
            @Override
            public void onSucceeded(CountryInfo countryInfo) {
                if (countryInfo != null) {
                    updateCountryInfo(countryInfo);
                }
            }

            @Override
            public void onFailed(String message) {
                Log.e(TAG, "Load country detail failed: " + message);
            }
        });
    }

    void updateCountryInfo(CountryInfo countryInfo) {
        if (countryInfo != null) {
            Locale currentLocale = getResources().getConfiguration().locale;
            String countryCode = currentLocale.getCountry().toLowerCase();

            String country = countryInfo.findTranslation(countryCode);
            if (country != null)
                tvCountry.setText(country);
            else
                tvCountry.setText(loadedCountry);

            tvCapital.setText(countryInfo.getCapital());
            tvCurrency.setText(countryInfo.getCurrency());
        }
    }
}
