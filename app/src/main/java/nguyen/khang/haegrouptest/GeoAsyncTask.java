package nguyen.khang.haegrouptest;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;

public class GeoAsyncTask extends AsyncTask<Void, Void, String>{
    String TAG = GeoAsyncTask.class.getSimpleName();

    WeakReference<Listener> listener;
    WeakReference<Context> context;
    Location location;

    public GeoAsyncTask(Context context, Location location) {
        this.context = new WeakReference<>(context);
        this.location = location;
    }

    @Override
    protected String doInBackground(Void... voids) {

        if (context.get() != null) {
            Geocoder geocoder = new Geocoder(context.get(), Locale.ENGLISH);
            List<Address> addresses = null;
            String errorMessage = "";

            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            } catch (IOException ioException) {
                errorMessage = "Service is not available";
                Log.e(TAG, errorMessage, ioException);
            } catch (IllegalArgumentException illegalArgumentException) {
                Log.e(TAG, "Invalid location");
            }

            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                String country = address.getCountryName();
                return country;
//                Log.i(TAG, "country: " + country + ", city: " + city);
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        if (listener != null && listener.get() != null) {
            listener.get().onFinish(s);
        }
    }

    public void setListener(Listener listener) {
        this.listener = new WeakReference<>(listener);
    }

    interface Listener {
        void onFinish(String country);
    }
}
