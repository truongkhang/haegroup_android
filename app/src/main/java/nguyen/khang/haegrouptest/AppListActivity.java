package nguyen.khang.haegrouptest;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.List;

public class AppListActivity extends AppCompatActivity {

    AppListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_list);

        setupView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        DataManager.getInstance().setAppLoadListener(null);
    }

    void setupView() {
        final RecyclerView recyclerView = findViewById(R.id.recycler_view);
        GridLayoutManager layoutManager;


        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            layoutManager = new GridLayoutManager(this, 4, GridLayoutManager.VERTICAL, false);
        else
            layoutManager = new GridLayoutManager(this, 6, GridLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);

        long start = System.currentTimeMillis();
        List<AppInfo> appList = DataManager.getInstance().loadAppList(this);
        Log.d("Test", "Time spent: " + (System.currentTimeMillis() - start));

        adapter = new AppListAdapter(appList);
        recyclerView.setAdapter(adapter);

        DataManager.getInstance().setAppLoadListener(new WeakReference<DataManager.AppLoadListener>(new DataManager.AppLoadListener() {
            @Override
            public void onAppInfoLoaded(final int position) {
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onAppIconLoaded(final int position) {
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyItemChanged(position);
                    }
                });
            }
        }));

        adapter.setListener(new AppListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(AppInfo item) {
                onAppClicked(item);
            }
        });
    }

    public void onAppClicked(AppInfo item) {
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(item.getPackageName());
        startActivity(launchIntent);
    }
}
