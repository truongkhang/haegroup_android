package nguyen.khang.haegrouptest;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import nguyen.khang.haegrouptest.api.CountryInfo;
import nguyen.khang.haegrouptest.api.ServiceApiProvider;
import nguyen.khang.haegrouptest.api.ServiceCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataManager {

    List<AppInfo> appList;
    WeakReference<AppLoadListener> appLoadListener;

    private static DataManager instance = new DataManager();

    public static DataManager getInstance() {
        return instance;
    }

    private DataManager() {
    }

    public List<AppInfo> loadAppList(final Context context) {
        if (appList == null) {
            appList = new ArrayList<AppInfo>();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    PackageManager pm = context.getPackageManager();
                    Intent intent = new Intent(Intent.ACTION_MAIN, null);
                    intent.addCategory(Intent.CATEGORY_LAUNCHER);

                    List<ResolveInfo> allApps = pm.queryIntentActivities(intent, 0);
                    for (int i = 0; i < allApps.size(); i++) {
                        ResolveInfo ri = allApps.get(i);
                        String appName = ri.loadLabel(pm).toString();
                        AppInfo app = new AppInfo(appName, ri.activityInfo);

                        appList.add(app);
                        if (appLoadListener != null && appLoadListener.get() != null) {
                            appLoadListener.get().onAppInfoLoaded(i);
                        }
                    }

                    for (int i = 0; i < appList.size(); i++) {
                        appList.get(i).loadIcon(context);
                        if (appLoadListener != null && appLoadListener.get() != null) {
                            appLoadListener.get().onAppIconLoaded(i);
                        }
                    }
                }
            }).start();
        }

        return appList;
    }

    public void loadCountryDetail(String name, final ServiceCallback<CountryInfo> callback) {
        final WeakReference<ServiceCallback<CountryInfo>> callbackWF = new WeakReference<ServiceCallback<CountryInfo>>(callback);
        String url = "https://restcountries.eu/rest/v2/name/" + name;
        ServiceApiProvider.getApiService().loadCountryInfo(url).enqueue(new Callback<List<CountryInfo>>() {
            @Override
            public void onResponse(Call<List<CountryInfo>> call, Response<List<CountryInfo>> response) {
                if (response.isSuccessful()) {
                    CountryInfo result = null;
                    if (response.body() != null && response.body().size() > 0)
                        result = response.body().get(0);

                    if (callbackWF.get() != null)
                        callbackWF.get().onSucceeded(result);
                } else {
                    if (callbackWF.get() != null) {
                        callbackWF.get().onFailed(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<CountryInfo>> call, Throwable t) {
                if (callbackWF.get() != null) {
                    callbackWF.get().onFailed(t.getMessage());
                }
            }
        });
    }

    public void setAppLoadListener(WeakReference<AppLoadListener> appLoadListener) {
        this.appLoadListener = appLoadListener;
    }

    public interface AppLoadListener {
        void onAppInfoLoaded(int position);

        void onAppIconLoaded(int position);
    }
}
