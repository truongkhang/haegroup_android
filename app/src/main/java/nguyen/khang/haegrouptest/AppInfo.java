package nguyen.khang.haegrouptest;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;

public class AppInfo {
    private String name;
    private String packageName;
    private Drawable icon;
    private ActivityInfo activityInfo;

    public AppInfo(String name, ActivityInfo activityInfo) {
        this.name = name;
        this.packageName = activityInfo.packageName;
        this.activityInfo = activityInfo;
    }

    public void loadIcon(Context context){
        icon = activityInfo.loadIcon(context.getPackageManager());
    }

    public String getName() {
        return name;
    }

    public String getPackageName() {
        return packageName;
    }

    public Drawable getIcon() {
        return icon;
    }
}

