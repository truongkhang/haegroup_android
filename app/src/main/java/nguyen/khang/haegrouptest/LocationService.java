package nguyen.khang.haegrouptest;

import android.Manifest;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LocationService extends Service {
    public static String LOG_CATEGORY = LocationService.class.getName();
    public static String INTENT_LOCATION = "LOCATION_SERVICE_UPDATE";
    public static String EXTRA_LOCATION = "EXTRA_LOCATION";

    LocationManager locationManager;
    Location curLocation;

    @Override
    public void onCreate() {
        super.onCreate();

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        registerReceiver(locationStatusReceiver, new IntentFilter("android.location.PROVIDERS_CHANGED"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            locationManager.removeUpdates(locationListener);
            unregisterReceiver(locationStatusReceiver);
        } catch (Exception e) {
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (isLocationEnable()) {
            loadLocation();
        }

        return START_STICKY;
    }

    void loadLocation() {
        Location lastLocation = null;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Log.i(LOG_CATEGORY, "last known location: " + (lastLocation != null ? lastLocation.toString() : ""));

        if (lastLocation != null) {
            curLocation =lastLocation;
            sendLocationUpdate();
        }

        // start listening
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10f, locationListener);
        Log.i(LOG_CATEGORY, "Location update requested");
    }

    void sendLocationUpdate() {
        Log.i(LOG_CATEGORY, "sendLocationUpdate: " + curLocation.toString());
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        Intent intent = new Intent(INTENT_LOCATION);
        intent.putExtra(EXTRA_LOCATION, curLocation);
        manager.sendBroadcast(intent);
    }

    boolean isLocationEnable() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.i(LOG_CATEGORY, "onLocationChanged: " + location.toString());

            curLocation = location;
            sendLocationUpdate();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.i(LOG_CATEGORY, "onStatusChanged: " + status);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.i(LOG_CATEGORY, "onProviderEnabled: " + provider);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.i(LOG_CATEGORY, "onProviderDisabled: " + provider);
        }
    };

    BroadcastReceiver locationStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean anyLocationProv = false;
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            anyLocationProv |= locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            anyLocationProv |= locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            Log.i("", "Location service status received, enable: " + anyLocationProv);

            if (anyLocationProv)
                loadLocation();
        }
    };

    public static boolean hasLocationPermission(Context context) {
        boolean canAccessFineLocation = ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        boolean canAccessCoarseLocation = ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        if (!canAccessFineLocation && !canAccessCoarseLocation) {
            return false;
        }

        return true;
    }
}
